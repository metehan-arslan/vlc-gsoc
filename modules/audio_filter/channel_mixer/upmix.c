/*****************************************************************************
 * upmix.c : Generic upmixing filter
 *****************************************************************************
 * Copyright © 2020 VLC authors and VideoLAN
 *
 * Authors: Vedanta Nayak <vedantnayak2@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_aout.h>
#include <vlc_filter.h>
#include <vlc_plugin.h>

#define NB_CHANNELS 4

#define UPMIX_TEXT N_( "Upmixing algorithm" )
#define UPMIX_LONGTEXT N_( "Algorithm to use for upmixing." )

enum {
    PASSIVE_SURROUND,
    PRINCIPAL_COMPONENT,
};

static const int algo_list[] = {
    PASSIVE_SURROUND    ,
    PRINCIPAL_COMPONENT ,
};
static const char *const algo_list_text[] = {
    N_("Passive Surround Decoding"), N_("Principal Component Analysis")
};

typedef struct
{
    float f_cen_l;
    float f_cen_r;
    float f_sur_l;
    float f_sur_r;
}filter_sys_t;

static block_t *PCA     ( filter_t *p_filter, block_t *p_in_buf )
{
    float *in = (float *)p_in_buf->p_buffer;
    filter_sys_t *p_sys = p_filter->p_sys;
    size_t i_nb_samples = p_in_buf->i_nb_samples;
    block_t *p_out_buf  = block_Alloc(sizeof(float) *
            i_nb_samples * NB_CHANNELS);
    if ( !p_out_buf )
        goto out;

    float * p_out = (float*)p_out_buf->p_buffer;
    p_out_buf->i_nb_samples = i_nb_samples;
    p_out_buf->i_dts        = p_in_buf->i_dts;
    p_out_buf->i_pts        = p_in_buf->i_pts;
    p_out_buf->i_length     = sizeof(float) * i_nb_samples;

    /*PCA uses the following covariance matrix
     *      | cov(xl,xl)    cov(xl,xr) |
     * A =  |                          |
     *      | cov(xr,xl)    cov(xr,xr) |
     *
     * where cov(xl,xr) = Sum( (xl[i] - xl(mean)) * (xr[i] - xr(mean)))/(n-1)
     *       given,   n = Number of samples
     *              Sum = Summation from i = 0 to n
     *
     * On further simplifying,
     *       cov(xl,xr) = Sum( xl[i]*xr[i] -xl(mean) * xr[i]
     *                      - xr(mean) * xl[i] + xr(mean) * xl(mean))/(n-1)
     *                  = (Sum(xl[i] * xr[i]) - Sum(xl(mean) * xr[i])
     *                      - Sum(xr(mean) * xl[i]) +
     *                      Sum(xr(mean) * xl(mean)))/(n-1)
     *                  = (Sum(xl[i] * xr[i]) - (xl(total) * xr(total))
     *                      - ((xr(total) * xl(total)) +
     *                      xl(total) * xr(total) )/( n - 1 )
     *                  = (Sum(xl[i] * xr[i]) - xl(total) * xr(total))/
     *                      (n-1)
     * Similarly,
     *       cov(xr,xl) = (Sum(xl[i] * xr[i]) - xl(total) * xr(total))/(n-1)
     *                  = cov(xl,xr)
     *       cov(xl,xl) = (Sum(xl[i] * xl[i]) - xl(total) * xl(total))/(n-1)
     *       cov(xl,xl) = (Sum(xr[i] * xr[i]) - xr(total) * xr(total))/(n-1)
     */
    float f_left_total = 0.0f, f_right_total = 0.0f;
    float f_pdt_lr = 0.0f, f_pdt_ll = 0.0f , f_pdt_rr = 0.0f;
    float f_center_left, f_center_right, f_surrnd_left, f_surrnd_right;
    for (size_t i = 0; i < i_nb_samples; ++i)
    {
        float f_left  = in [i * 2];
        float f_right = in [i * 2 + 1];
        p_out [i * NB_CHANNELS]     = f_left;
        p_out [i * NB_CHANNELS + 1] = f_right;
        f_left_total     += f_left;
        f_right_total    += f_right;
        f_pdt_lr         += f_left * f_right;
        f_pdt_ll         += f_left * f_left;
        f_pdt_rr         += f_right * f_right;
    }

    /*cov (xl,xl) */
    float f_cov_ll = (f_pdt_ll - f_left_total * f_left_total)/
        (i_nb_samples - 1);
    /*cov (xl,xr) = cov (xr,xl) */
    float f_cov_lr = (f_pdt_lr - f_right_total * f_left_total)/
        (i_nb_samples - 1);
    /*cov (xl,xl) */
    float f_cov_rr = (f_pdt_rr - f_right_total * f_right_total)/
        (i_nb_samples - 1);

    /*Calculating the magnitude */
    float f_mag = f_cov_ll * f_cov_ll + f_cov_lr * f_cov_lr -
        f_cov_lr * f_cov_lr + f_cov_rr * f_cov_rr ;

    /*Assign center and surround vector */
    if (f_mag >= 0)
    {
        f_center_left = f_cov_ll;
        f_center_right= f_cov_lr;
        f_surrnd_left = f_cov_lr;
        f_surrnd_right= f_cov_rr;
    }
    else
    {
        f_center_left = f_cov_lr;
        f_center_right= f_cov_rr;
        f_surrnd_left = f_cov_ll;
        f_surrnd_right= f_cov_lr;
    }
    size_t i_quarter = i_nb_samples >> 6;
    for ( size_t i = 0 ; i < i_nb_samples ; ++i)
    {
        float f_left        = in[ i * 2];
        float f_right       = in[ i * 2 + 1];

        float f_coeff       = (i/i_quarter) * 0.015625;
        float f_cencoeff_l  = p_sys->f_cen_l * (1 - f_coeff) +
            f_center_left * f_coeff;
        float f_cencoeff_r  = p_sys->f_cen_r * (1 - f_coeff) +
            f_center_right * f_coeff;
        float f_surcoeff_l  = p_sys->f_sur_l * (1 - f_coeff) +
            f_surrnd_left * f_coeff;
        float f_surcoeff_r  = p_sys->f_sur_r * (1 - f_coeff) +
            f_surrnd_right * f_coeff;

        float f_center    = f_cencoeff_l * f_left + f_cencoeff_r * f_right;
        float f_surround  = f_surcoeff_l * f_left + f_surcoeff_r * f_right;
        p_out[i * NB_CHANNELS   ]   = f_left;
        p_out[i * NB_CHANNELS + 1 ] = f_right;
        p_out[i * NB_CHANNELS + 2 ] = f_surround;
        p_out[i * NB_CHANNELS + 3 ] = f_center;
    }
    p_sys->f_cen_l = f_center_left;
    p_sys->f_cen_r = f_center_right;
    p_sys->f_sur_l = f_surrnd_left;
    p_sys->f_sur_r = f_surrnd_right;
out:
    block_Release(p_in_buf);
    return p_out_buf;
}

static block_t *PassiveSurround ( filter_t * p_filter, block_t * p_in_buf )
{
    (void) p_filter;
    float *p_in = (float*) p_in_buf->p_buffer;
    size_t i_nb_samples = p_in_buf->i_nb_samples;
    block_t *p_out_buf = block_Alloc(sizeof(float) * i_nb_samples * NB_CHANNELS);
    if ( !p_out_buf )
        goto out;
    float * p_out           = (float*)p_out_buf->p_buffer;
    p_out_buf->i_nb_samples = i_nb_samples;
    p_out_buf->i_dts        = p_in_buf->i_dts;
    p_out_buf->i_pts        = p_in_buf->i_pts;
    p_out_buf->i_length     = sizeof(float) * i_nb_samples;
    for ( size_t i = 0 ; i < i_nb_samples ; ++i)
    {
        float f_left      = p_in[i * 2 ];
        float f_right     = p_in[i * 2 + 1 ];
        float f_center    = ( f_left + f_right ) / 2;
        float f_surround  = ( f_left - f_right ) / 2;

        p_out[i * NB_CHANNELS   ]   = f_left;
        p_out[i * NB_CHANNELS + 1 ] = f_right;
        p_out[i * NB_CHANNELS + 2 ] = f_surround;
        p_out[i * NB_CHANNELS + 3 ] = f_center;
    }
out:
    block_Release(p_in_buf);
    return p_out_buf;
}

static int Open (vlc_object_t *p_in)
{
    filter_t *p_filter = (filter_t *)p_in;
    if (p_filter->fmt_in.audio.i_physical_channels != AOUT_CHANS_STEREO)
        return VLC_EGENERIC;

    static_assert(AOUT_CHANIDX_CENTER > AOUT_CHANIDX_REARCENTER
            && AOUT_CHANIDX_REARCENTER > AOUT_CHANIDX_RIGHT &&
        AOUT_CHANIDX_RIGHT > AOUT_CHANIDX_LEFT, "Change in channel order.");

    p_filter->fmt_out.audio.i_format = VLC_CODEC_FL32;
    p_filter->fmt_in.audio.i_format = VLC_CODEC_FL32;
    p_filter->fmt_out.audio.i_physical_channels = AOUT_CHANS_4_CENTER_REAR;
    p_filter->fmt_out.audio.i_rate = p_filter->fmt_in.audio.i_rate;
    aout_FormatPrepare(&p_filter->fmt_in.audio);
    aout_FormatPrepare(&p_filter->fmt_out.audio);
    size_t algorithm = var_InheritInteger( vlc_object_parent(p_filter),
              "upmixing-algo" );
    switch(algorithm)
    {
        case PASSIVE_SURROUND:
            p_filter->pf_audio_filter=PassiveSurround;
            break;
        case PRINCIPAL_COMPONENT : ;
            filter_sys_t * p_sys;
            p_sys = p_filter->p_sys = malloc(sizeof(*p_sys));
            p_sys->f_cen_l = 0.0f;
            p_sys->f_cen_r = 0.0f;
            p_sys->f_sur_l = 0.0f;
            p_sys->f_sur_r = 0.0f;
            p_filter->pf_audio_filter = PCA;
            break;
    }
    return VLC_SUCCESS;
}

static void Close( vlc_object_t *p_this )
{
    filter_t * p_filter = (filter_t *)p_this;
    filter_sys_t * p_sys= p_filter->p_sys;
    free(p_sys);
}

vlc_module_begin()
    set_shortname (N_("General Upmixer"))
    set_description (N_("Upmix Stereo to 4.0 surround audio"))
    add_integer( "upmixing-algo", algo_list [0], UPMIX_TEXT,
            UPMIX_LONGTEXT, false)
        change_integer_list( algo_list , algo_list_text )
    set_category (CAT_AUDIO)
    set_subcategory (SUBCAT_AUDIO_AFILTER)
    set_capability ("audio filter",0)
    set_callbacks (Open,Close)
vlc_module_end ()
